# Solitaire

Solitaire's purpose is demonstrate object oriented design's principles / patterns. I also want to learn to write clean code. After all
(hopefully soon) I will write Java code to C++ and make cross-platform mobile application with Djinni.

### Commands
move

expose

draw

cancel

quit

### UNIX
![alt tag](https://bitbucket.org/siimooo/solitaire/raw/master/linux.png)

### Windows
![alt tag](https://bitbucket.org/siimooo/solitaire/raw/master/windows.png)

> Simo Ala-Kotila

### Version
0.2

License
----
MIT
Free Software!

