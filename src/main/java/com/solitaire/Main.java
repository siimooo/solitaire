package com.solitaire;

import com.solitaire.engine.command.cor.Machine;
import com.solitaire.engine.command.cor.Request;
import com.solitaire.engine.deck.Deck;
import com.solitaire.engine.game.player.HumanPlayer;
import com.solitaire.engine.game.table.Table;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 10.7.2015
 * Time: 10.10
 */
public class Main {

    public static void main(String[] args) throws InterruptedException, IOException {

        HumanPlayer player = new HumanPlayer("Simo");
        Table table = new SolitaireTable();
        table.setDealer(new SolitaireDealer());
        table.addMember(player);

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String command;
        Machine machine = new Machine();

        while (!table.getDealer().isWinner()) {
            System.out.println("Write command and press 'Enter' ");
            command = br.readLine();
            if(!isEmpty(command)){
                machine.makeRequest(new Request(command, player, table));
            }
        }
    }

    public static boolean isEmpty(String command){
        return command == null || "".equals(command);
    }
}
