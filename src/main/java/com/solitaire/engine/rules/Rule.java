package com.solitaire.engine.rules;

import com.solitaire.engine.card.Card;
import com.solitaire.engine.deck.Deck;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 16.6.2015
 * Time: 14.28
 */
public abstract class Rule implements Validate {

    private final Deck mTo;

    public Rule(Deck to){
        mTo = to;
    }

    public boolean isLowerByOneRank(Card from, Card to){
        int fromRank = from.ranks().getValue();
        int toRank= to.ranks().getValue();
        return (fromRank + 1) == toRank;
    }

    public boolean isSameColor(Card from, Card to){
        return from.color() == to.color();
    }

    public boolean isSameSuits(Card from, Card to){
        return from.suits() == to.suits();
    }

    public boolean isValidMergeStack(Deck merge){
        for(int i = 0; i < merge.amount(); i++){

            Card first = merge.showCard(i);

            if(!first.isVisible()){
                System.out.println("invalid merge stack!");
            }

            if(i + 1 < merge.amount()){
                Card second = merge.showCard(i + 1);
                if(!second.isVisible()){
                    System.out.println("invalid merge stack!");
                }

                if(!isLowerByOneRank(first, second)){
                    System.out.println("invalid merge stack! rank at " + i + ", -> " + first + second);
                    return false;
                }

                if(isSameColor(first, second)){
                    System.out.println("invalid merge stack! same platform at " + i + ", -> " + first + second);
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean validate(Deck merge) {
        if(!isValidMergeStack(merge)){
            return false;
        }

        if(mTo.isEmpty()){
            return true;
        }

        if(!mTo.showCard().isVisible()){
            System.out.println("Moving to deck which first card must be visible");
            return false;
        }


        Card mergeLastCard = merge.showCard(merge.lastIndex());

        if(!mergeLastCard.isVisible()){
            System.out.println("Card must be visible");
            return false;
        }

        if(!isLowerByOneRank(mergeLastCard, mTo.showCard())){
            System.out.println("invalid rank " + mergeLastCard.ranks());
            return false;
        }

        if(isSameColor(mergeLastCard, mTo.showCard())){
            System.out.println("invalid platform. Is it same " + mergeLastCard.color());
            return false;
        }


        return true;
    }

    public Deck getTo(){
        return mTo;
    }

}
