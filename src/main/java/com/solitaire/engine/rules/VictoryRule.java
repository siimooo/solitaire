package com.solitaire.engine.rules;

import com.solitaire.contract.enums.Ranks;
import com.solitaire.engine.card.Card;
import com.solitaire.engine.deck.Deck;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 16.6.2015
 * Time: 14.39
 */
public class VictoryRule extends Rule {

    public VictoryRule(Deck to) {
        super(to);
    }

    @Override
    public boolean validate(Deck merge) {
        if(merge.amount() > 1){
            System.out.println("Only one card is possible");
            return false;
        }

        Card card = merge.showCard();

        if(!card.isVisible()){
            System.out.println("Card must be visible");
            return false;
        }

        if(getTo().isEmpty() && !isAce(card)){//first card must be ace
            System.out.println("First card must be ace");
            return false;
        }

        if(getTo().isEmpty() && isAce(card)){
            return true;
        }

        if(!isSameSuits(card, getTo().showCard())){
            System.out.println("Card must be same suit");
            return false;
        }

        if(!isIncremental(card)){
            System.out.println("card must be only one higher rank");
            return false;
        }


        return true;
    }

    public boolean isAce(Card card){
        return card.ranks() == Ranks.ACE;
    }

    public boolean isHigherByOne(Card coming, Card last){
        int comingRank = coming.ranks().getValue();
        int lastRank = last.ranks().getValue();
        return (lastRank + 1) == comingRank;
    }


    public boolean isIncremental(Card card){
        Card last = getTo().showCard(getTo().lastIndex());
        if(!isHigherByOne(card, last)){
            return false;
        }
        return true;
    }

}