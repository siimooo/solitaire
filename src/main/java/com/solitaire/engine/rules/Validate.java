package com.solitaire.engine.rules;

import com.solitaire.engine.deck.Deck;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 16.6.2015
 * Time: 14.41
 */
public interface Validate {
    
    boolean validate(Deck merge);
}
