package com.solitaire.engine.rules;

import com.solitaire.engine.deck.Deck;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 16.6.2015
 * Time: 14.37
 */
public class MainRule extends Rule{

    public MainRule(Deck to) {
        super(to);
    }

    @Override
    public boolean validate(Deck merge) {
        if(merge.amount() > 1){
            System.out.println("Allowed take only one card");
            return false;
        }

        return super.validate(merge);
    }

}
