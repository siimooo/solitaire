package com.solitaire.engine.rules;

/**
 * This class tells to program how to display cards
 *
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 05.06.2015
 * Time: 13.29
 */
public class Display {

    private final int mDisplay;
    private final boolean mPrintOnStart;

    public Display(int display, boolean printOnStart){
        mDisplay = display;
        mPrintOnStart = printOnStart;
    }

    /**
     * How many visible card is in main deck
     *
     * @return count
     */
    public int getDisplayCount(){
        return mDisplay;
    }

    /**
     * Does user want display main cards on start
     *
     * @return
     */
    public boolean printOnStart(){
        return mPrintOnStart;
    }
}
