package com.solitaire.engine.rules;

import com.solitaire.engine.deck.Deck;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 16.6.2015
 * Time: 14.38
 */
public class StackRule extends Rule{

    public StackRule(Deck to) {
        super(to);
    }

    @Override
    public boolean validate(Deck merge) {
        return super.validate(merge);
    }
}
