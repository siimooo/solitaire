package com.solitaire.engine.game;

import com.solitaire.contract.enums.Ranks;
import com.solitaire.contract.enums.Suits;
import com.solitaire.engine.card.*;
import com.solitaire.engine.deck.BottomDeck;
import com.solitaire.engine.deck.Deck;
import com.solitaire.engine.deck.MainDeck;
import com.solitaire.engine.deck.SuitsDeck;

/**
 * Created by Simo on 01.06.2015.
 */
public abstract class Dealer{

    /**
     * Ranks contains all possible card types
     */
    private final Ranks[] mRanks = {Ranks.TWO, Ranks.THREE, Ranks.FOUR, Ranks.FIVE, Ranks.SIX, Ranks.SEVEN, Ranks.EIGHT, Ranks.NINE, Ranks.TEN, Ranks.JACK, Ranks.QUEEN, Ranks.KING, Ranks.ACE, Ranks.JOKER};

    /**
     * Suits contains four popular types and also joker
     */
    private final Suits[] mSuits = {Suits.CLUBS, Suits.DIAMONDS, Suits.HEARTS, Suits.SPADES, Suits.JOKER};

    private Deck[] mDecks;

    public Dealer(){
        Deck deck = setUp();
        fill(deck);
    }

    public abstract void setWinner();

    public abstract void fill(Deck deck);

    public abstract Deck setUp();

    public abstract boolean isWinner();

    public abstract void playerIn();

    public abstract void playersCheck();

    /**
     * Create card concrete class based on suits.
     *
     * @param rank enum value of rank
     * @param suits enum value of suit
     *
     * @return card
     */
    private Card getCard(Ranks rank, Suits suits) {
        if (suits == Suits.CLUBS) {
            return new Clubs(rank);
        } else if (suits == Suits.DIAMONDS) {
            return new Diamonds(rank);
        } else if (suits == Suits.HEARTS) {
            return new Hearts(rank);
        } else if (suits == Suits.SPADES) {
            return new Spades(rank);
        } else if (suits == Suits.JOKER) {
            return new Joker(Ranks.JOKER);
        } else {
            throw new IllegalArgumentException("Unsupported card type");
        }
    }

    /**
     * Build 52-card the most popular deck.
     *
     * @return stack of cards
     */
    public Deck buildCardDeck() {
        Deck deck = new Deck("Deck", 52);
        for (int s = 0; s < 4; s++) {
            Suits suit = mSuits[s];
            for (int r = 0; r < 13; r++) {
                Ranks rank = mRanks[r];
                Card card = getCard(rank, suit);
                deck.addComponent(card);
            }
        }
        return deck;
    }

    public Deck[] getDecks(){
        return mDecks;
    }

    public void setDecks(Deck[] decks){
        mDecks = decks;
    }

}
