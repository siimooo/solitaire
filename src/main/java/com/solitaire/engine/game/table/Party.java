package com.solitaire.engine.game.table;

import com.solitaire.engine.command.Action;
import com.solitaire.engine.game.player.PartyMember;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 28.6.2015
 * Time: 12.31
 */
public abstract class Party {

    private List<PartyMember> mMembers = new ArrayList<PartyMember>();

    public void addMember(PartyMember member){
        mMembers.add(member);
        member.joinedParty(this);
    }

    public void act(PartyMember sender, Action action){
        for (PartyMember member : mMembers) {
            if (sender != member) {
                member.action(action);
            }
        }
    }
}
