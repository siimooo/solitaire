package com.solitaire.engine.game.table;

import com.solitaire.SolitaireDealer;
import com.solitaire.engine.command.Action;
import com.solitaire.engine.deck.Deck;
import com.solitaire.engine.game.Dealer;
import com.solitaire.engine.game.player.PartyMember;
import com.solitaire.engine.platform.AbstractPlatform;
import com.solitaire.engine.platform.AbstractTableColor;
import com.solitaire.engine.platform.Detector;
import com.solitaire.engine.platform.unix.Unix;
import com.solitaire.engine.platform.windows.Windows;
import com.solitaire.engine.rules.Score;

import java.util.ArrayList;
import java.util.List;

public abstract class Table extends Party {

    private Score mScore;

    private Dealer mDealer;

    private AbstractTableColor mAbstractTableColor;

    public Table(){
        detectOS();
    }

    public void detectOS() {
        Detector detector = new Detector();
        mAbstractTableColor = detector.detectOS().makeTableColor();
    }

    public Dealer getDealer(){
        return mDealer;
    }

    public void setDealer(Dealer dealer){
        mDealer = dealer;
    }

    public void draw() {
        for(Deck deck : mDealer.getDecks()){
            deck.draw();
        }
    }

    public AbstractTableColor getAbstractTableColor(){
        return mAbstractTableColor;
    }
}
