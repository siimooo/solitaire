package com.solitaire.engine.game.player;

import com.solitaire.engine.deck.Deck;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 16.6.2015
 * Time: 11.57
 */
public class HumanPlayer extends Player{

    public HumanPlayer(String name) {
        super(name);
    }

    @Override
    public void moveCard(int index, Deck from, Deck to) {
        index = Math.abs((from.amount() - 1) - index);
        super.moveCard(index, from, to);
    }
}
