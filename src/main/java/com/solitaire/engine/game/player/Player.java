package com.solitaire.engine.game.player;

import com.solitaire.engine.command.*;
import com.solitaire.engine.deck.Deck;

public abstract class Player extends PartyMemberBase {

    private final String mName;

    public Player(String name) {
        mName = name;
    }

    public void moveCard(int index, Deck from, Deck to){
        MoveCommand move = new MoveCommand(index, from, to);
        move.execute();

        if(!isInParty()){
            return;
        }

        act(move);
    }

    public void undo(){

    }

    public void redo(){

    }

    public void clickDeck(Deck deck){
        ClickCommand click = new ClickCommand(deck);
        click.execute();
    }

    public String getName() {
        return mName;
    }

}
