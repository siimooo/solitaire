package com.solitaire.engine.game.player;

import com.solitaire.engine.command.Command;
import com.solitaire.engine.command.Action;
import com.solitaire.engine.game.table.Party;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 29.6.2015
 * Time: 10.20
 */
public interface PartyMember {

    void joinedParty(Party party);

    void action(Action action);

    void act(Command command);
}
