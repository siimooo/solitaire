package com.solitaire.engine.game.player;

import com.solitaire.engine.command.Command;
import com.solitaire.engine.command.Action;
import com.solitaire.engine.game.table.Party;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 29.6.2015
 * Time: 10.21
 */
public abstract class PartyMemberBase implements PartyMember {

    private Party mParty;

    private boolean mIsInParty = false;

    @Override
    public void joinedParty(Party party) {
        mParty = party;
        mIsInParty = true;
    }

    @Override
    public void action(Action action) {
//        System.out.println("show command to all other players");
    }

    @Override
    public void act(Command command) {
//        System.out.println("Send command to all other players");
        mParty.act(this, command.getAction());
    }

    public boolean isInParty(){
        return mIsInParty;
    }

}
