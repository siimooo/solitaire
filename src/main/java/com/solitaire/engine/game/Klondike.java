package com.solitaire.engine.game;

import com.solitaire.engine.rules.Display;

/**
 * Created by Simo on 29.05.2015.
 */
public class Klondike {

    private Display mDisplay;

    public Klondike(){
        mDisplay = new Display(3, true);
    }

    public Display getDisplay(){
        return mDisplay;
    }
}
