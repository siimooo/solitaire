package com.solitaire.engine.platform;

import com.solitaire.engine.platform.unix.Unix;
import com.solitaire.engine.platform.windows.Windows;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 24.7.2015
 * Time: 15.57
 */
public class Detector {

    public AbstractPlatform detectOS(){
        if(AbstractPlatform.isWindows()){
            return new Windows();
        }else if(AbstractPlatform.isUnix()){
            return new Unix();
        }else{
            throw new RuntimeException("Unknown os " + AbstractPlatform.getOperatingSystemType());
        }
    }
}
