package com.solitaire.engine.platform;

import com.solitaire.engine.card.Card;

import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 8.7.2015
 * Time: 11.35
 */
public abstract class AbstractPlatform {

    public abstract AbstractCardColor  makeCardColor(Card card);

    public abstract AbstractTableColor makeTableColor();

    /**
     * types of Operating Systems
     */
    public enum OSType {
        Windows, MacOS, Linux, Other
    }

    public static OSType mDetectedOS;


    public static boolean isWindows(){
        if(mDetectedOS == null){
            mDetectedOS = getOperatingSystemType();
        }
        return mDetectedOS == OSType.Windows;
    }

    public static boolean isUnix(){
        if(mDetectedOS == null){
            mDetectedOS = getOperatingSystemType();
        }
        return mDetectedOS == OSType.MacOS || mDetectedOS == OSType.Linux;
    }

    public static boolean isUnknown(){
        return mDetectedOS == OSType.Other;
    }

    public static OSType getOperatingSystemType() {
        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        if ((OS.indexOf("mac") >= 0) || (OS.indexOf("darwin") >= 0)) {
            mDetectedOS = OSType.MacOS;
        } else if (OS.indexOf("win") >= 0) {
            mDetectedOS = OSType.Windows;
        } else if (OS.indexOf("nux") >= 0) {
            mDetectedOS = OSType.Linux;
        } else {
            mDetectedOS = OSType.Other;
        }
        return mDetectedOS;
    }

}
