package com.solitaire.engine.platform.windows;

import com.solitaire.engine.card.Card;
import com.solitaire.engine.platform.AbstractCardColor;
import com.solitaire.engine.platform.AbstractPlatform;
import com.solitaire.engine.platform.AbstractTableColor;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 26.6.2015
 * Time: 09.47
 */
public class Windows extends AbstractPlatform {

    @Override
    public AbstractCardColor makeCardColor(Card card) {
        return new WindowsCardColor(card);
    }

    @Override
    public AbstractTableColor makeTableColor() {
        return new WindowTableColor();
    }
}
