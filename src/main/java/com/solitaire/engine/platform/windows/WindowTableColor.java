package com.solitaire.engine.platform.windows;

import com.solitaire.engine.platform.AbstractTableColor;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 24.7.2015
 * Time: 15.21
 */
public class WindowTableColor extends AbstractTableColor {

    @Override
    public void clear() {
        //Do nothing because windows not support ASCII
    }
}
