package com.solitaire.engine.platform.windows;

import com.solitaire.contract.enums.Ranks;
import com.solitaire.contract.enums.Suits;
import com.solitaire.engine.card.Card;
import com.solitaire.engine.platform.AbstractCardColor;
import com.solitaire.engine.platform.AnsiColor;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 8.7.2015
 * Time: 11.37
 */
public class WindowsCardColor extends AbstractCardColor {

    public WindowsCardColor(Card card) {
        super(card);
    }

    @Override
    public void applySuitColor() {
        System.out.printf("%1s ", getCard().suits());
    }

    @Override
    public void applyRankColor() {
        System.out.printf("%1s ", getCard().ranks() + " " + getCard().color() + ",");
    }

    @Override
    public void applyUnVisibleColor() {
        System.out.printf(" %1s", "*");
    }
}
