package com.solitaire.engine.platform.unix;

import com.solitaire.engine.platform.AbstractCardColor;
import com.solitaire.engine.platform.AbstractTableColor;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 24.7.2015
 * Time: 15.13
 */
public class UnixTableColor extends AbstractTableColor{

    @Override
    public void clear() {
        System.out.print("\033[H\033[2J");
    }
}
