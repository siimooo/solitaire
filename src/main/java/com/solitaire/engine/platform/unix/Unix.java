package com.solitaire.engine.platform.unix;

import com.solitaire.engine.card.Card;
import com.solitaire.engine.platform.AbstractCardColor;
import com.solitaire.engine.platform.AbstractPlatform;
import com.solitaire.engine.platform.AbstractTableColor;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 26.6.2015
 * Time: 10.08
 */
public class Unix extends AbstractPlatform{

    @Override
    public AbstractCardColor makeCardColor(Card card) {
        return new UnixCardColor(card);
    }

    @Override
    public AbstractTableColor makeTableColor() {
        return new UnixTableColor();
    }
}
