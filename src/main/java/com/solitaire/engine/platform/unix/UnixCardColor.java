package com.solitaire.engine.platform.unix;

import com.solitaire.contract.enums.Ranks;
import com.solitaire.contract.enums.Suits;
import com.solitaire.engine.card.Card;
import com.solitaire.engine.platform.AbstractCardColor;
import com.solitaire.engine.platform.AnsiColor;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 8.7.2015
 * Time: 11.40
 */
public class UnixCardColor extends AbstractCardColor {

    public UnixCardColor(Card card) {
        super(card);
    }

    @Override
    public void applySuitColor() {
        if(isRed()){
            System.out.printf("%1s ", AnsiColor.ANSI_RED + getCard().suits().getValue() + AnsiColor.ANSI_RESET);
        }else{
            System.out.printf("%1s ", AnsiColor.ANSI_BLACK + getCard().suits().getValue() + AnsiColor.ANSI_RESET);
        }
    }

    @Override
    public void applyRankColor() {
        System.out.printf("%1s ", AnsiColor.ANSI_WHITE + getCard().ranks() + AnsiColor.ANSI_RESET);
    }

    @Override
    public void applyUnVisibleColor() {
        System.out.printf(" %1s", AnsiColor.ANSI_BLACK + "*" + AnsiColor.ANSI_RESET);
    }
}
