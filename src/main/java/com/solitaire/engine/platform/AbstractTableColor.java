package com.solitaire.engine.platform;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 24.7.2015
 * Time: 15.13
 */
public abstract class AbstractTableColor {

    public abstract void clear();
}
