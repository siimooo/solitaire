package com.solitaire.engine.platform;

import com.solitaire.contract.enums.Color;
import com.solitaire.contract.enums.Ranks;
import com.solitaire.contract.enums.Suits;
import com.solitaire.engine.card.Card;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 26.6.2015
 * Time: 09.46
 */
public abstract class AbstractCardColor {

    private final Card mCard;

    public AbstractCardColor(Card card){
        mCard = card;
    }

    abstract public void applySuitColor();

    abstract public void applyRankColor();

    abstract public void applyUnVisibleColor();

    public boolean isRed(){
        return mCard.color() == Color.RED;
    }

    public boolean isBlack(){
        return mCard.color() == Color.BLACK;
    }

    public Card getCard(){
        return mCard;
    }
}
