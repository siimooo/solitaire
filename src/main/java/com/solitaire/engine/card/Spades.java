package com.solitaire.engine.card;

import com.solitaire.contract.enums.Color;
import com.solitaire.contract.enums.Ranks;
import com.solitaire.contract.enums.Suits;

/**
 * Implements the "lowest common denominator"
 *
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 25.05.2015
 * Time: 13.40
 */
public class Spades extends Card {

    public Spades(Ranks ranks) {
        super(ranks);
    }

    public Spades(Ranks ranks, boolean visible) {
        super(ranks, visible);
    }

    @Override
    public Suits suits() {
        return Suits.SPADES;
    }

    @Override
    public Color color() {
        return Color.BLACK;
    }
}
