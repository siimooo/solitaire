package com.solitaire.engine.card;

import com.solitaire.contract.enums.Color;
import com.solitaire.contract.enums.Ranks;
import com.solitaire.contract.enums.Suits;

/**
 * Implements the "lowest common denominator"
 *
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 25.05.2015
 * Time: 13.40
 */
public class Joker extends Card{

    public Joker(Ranks ranks) {
        super(Ranks.JOKER);
    }

    public Joker(Ranks ranks, boolean visible) {
        super(Ranks.JOKER, visible);
    }

    public Joker(){
        super(Ranks.JOKER);
    }

    @Override
    public Suits suits() {
        return Suits.JOKER;
    }

    @Override
    public Color color() {
        return Color.BLACK;
    }

}
