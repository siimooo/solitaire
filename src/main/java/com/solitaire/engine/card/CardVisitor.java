package com.solitaire.engine.card;

import com.solitaire.contract.Visitor;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 9.6.2015
 * Time: 11.22
 */
public class CardVisitor implements Visitor {

    private Card mCard;

    @Override
    public void visit(Card card) {
        mCard = card;
    }

    public Card getCard(){
        return mCard;
    }
}
