package com.solitaire.engine.card;

import com.solitaire.contract.Component;
import com.solitaire.contract.Visitor;
import com.solitaire.contract.enums.Color;
import com.solitaire.contract.enums.Ranks;
import com.solitaire.contract.enums.Suits;
import com.solitaire.engine.platform.AbstractCardColor;
import com.solitaire.engine.platform.AbstractPlatform;
import com.solitaire.engine.platform.Detector;
import com.solitaire.engine.platform.unix.Unix;
import com.solitaire.engine.platform.windows.Windows;

/**
 * Abstract "lowest common denominator"
 *
 * @author Simo Ala-Kotila
 */
public abstract class Card implements Component, Comparable<Card>{

    private AbstractCardColor mAbstractCardColor;

    /**
     * Immutable rank value
     */
    private final Ranks mRank;

    /**
     * Flag for card, determine possible to show card value or not. default is not visible
     */
    private boolean mIsVisible = false;

    public Card(Ranks ranks) {
        mRank = ranks;
        detectOS();
    }

    public Card(Ranks ranks, boolean visible) {
        mRank = ranks;
        mIsVisible = visible;
        detectOS();
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public boolean isVisible(){
        return mIsVisible;
    }

    public void setVisible(boolean visible){
         mIsVisible = visible;
    }


    /**
     * Get card suit
     *
     * @return suits enum of card
     */
    public abstract Suits suits();

    /**
     * Get card platform
     *
     * @return platform enum of card
     */
    public abstract Color color();

    @Override
    public void detectOS() {
        Detector detector = new Detector();
        mAbstractCardColor = detector.detectOS().makeCardColor(this);
    }

    @Override
    public void draw() {
        if(isVisible()){
            mAbstractCardColor.applySuitColor();
            mAbstractCardColor.applyRankColor();
        }else{
            mAbstractCardColor.applyUnVisibleColor();
        }
    }

    /**
     * Get card rank
     *
     * @return ranks enum of card
     */
    public Ranks ranks() {
        return mRank;
    }

    @Override
    public int compareTo(Card card) {
        if(mRank.getValue() > card.ranks().getValue()){//more than
            return 1;
        }else if(mRank.getValue() < card.ranks().getValue()){//less than
            return -1;
        }else{//equal
            return 0;
        }
    }

    public String toString(){
        return suits().getValue() + " " + ranks() + ", " + color().getValue() + ". ";
    }
}
