package com.solitaire.engine.card;

import com.solitaire.contract.enums.Color;
import com.solitaire.contract.enums.Ranks;
import com.solitaire.contract.enums.Suits;

/**
 * Implements the "lowest common denominator"
 *
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 25.05.2015
 * Time: 13.40
 */
public class Hearts extends Card{

    public Hearts(Ranks ranks) {
        super(ranks);
    }

    public Hearts(Ranks ranks, boolean visible) {
        super(ranks, visible);
    }

    @Override
    public Suits suits() {
        return Suits.HEARTS;
    }

    @Override
    public Color color() {
        return Color.RED;
    }

}
