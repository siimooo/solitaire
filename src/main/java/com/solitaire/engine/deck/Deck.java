package com.solitaire.engine.deck;

import com.solitaire.engine.card.CardVisitor;
import com.solitaire.engine.card.Card;
import com.solitaire.contract.Component;
import com.solitaire.contract.Composite;
import com.solitaire.contract.Visitor;
import com.solitaire.contract.FixedSteque;
import com.solitaire.engine.platform.AbstractPlatform;
import com.solitaire.engine.platform.unix.Unix;
import com.solitaire.engine.platform.windows.Windows;
import com.solitaire.engine.rules.MainRule;
import com.solitaire.engine.rules.Validate;

import java.util.ArrayList;
import java.util.List;

/**
 * Deck implements the "lowest common denominator"
 */
public class Deck extends Composite implements Validate {

    private AbstractPlatform mAbstractPlatform;

    public Deck(String name, int size) {
        super(name, size);
        detectOS();
    }

    /**
     * Visit deck stack first item
     *
     * @param visitor implement
     */
    @Override
    public void accept(Visitor visitor) {
        accept(visitor, 0);
    }

    @Override
    public void detectOS() {
        AbstractPlatform abstractPlatform;
        if(AbstractPlatform.isWindows()){
            abstractPlatform = new Windows();
        }else if(AbstractPlatform.isUnix()){
            abstractPlatform = new Unix();
        }else{
            throw new RuntimeException("Unknown os " + AbstractPlatform.getOperatingSystemType());
        }
    }

    /**
     * Determine which index of stack want to visit
     *
     * @param visitor implement
     * @param index number of index
     */
    public void accept(Visitor visitor, int index) {
        getComponent(index).accept(visitor);
    }


    public int lastIndex(){
        if(isEmpty()){
            return 0;
        }
        return amount() - 1;
    }


    /**
     * Pop out card from deck
     *
     * @return card
     */
    public Card takeCard(){
        CardVisitor visitor = new CardVisitor();
        getComponents().pop().accept(visitor);
        return visitor.getCard();
    }

    public boolean isEmpty(){
        return getComponents().isEmpty();
    }

    /**
     * Peek card from deck. Won't remove it from deck
     *
     * @return card
     */
    public Card showCard(){
        CardVisitor visitor = new CardVisitor();
        getComponents().peek().accept(visitor);
        return visitor.getCard();
    }

    /**
     * Peek card from deck by index. Won't remove it from deck
     *
     * @param index given index number
     *
     * @return card
     */
    public Card showCard(int index){
        CardVisitor visitor = new CardVisitor();
        Component component = getComponent(index);
        component.accept(visitor);
        return visitor.getCard();
    }

    /**
     * Size of this deck
     *
     * @return amount of size
     */
    public int amount(){
        return getComponents().size();
    }


    /**
     *
     * @param last is zero based index
     * @return
     */
    public Deck popsCard(int last) {
        FixedSteque<Component> components = getComponents();

        if(last > (components.size() - 1) || last < 0){
            throw new IllegalArgumentException("invalid last size request -> "+ last +" deck size is " + amount());
        }

        int size = components.size() - last;
        Deck temp = new Deck("Temp", size);

        int compSize = components.size();
        for(int i = last; i < compSize; i++){
            temp.addComponent(components.pop());
        }

        return temp;
    }

    /**
     * Expose will change visibility of card
     */
    public void expose(){
        if(!showCard().isVisible()){
            showCard().setVisible(true);
        }
    }

    @Override
    public void draw() {
        for(Component component : getComponents()){
            component.draw();
        }
        empty();
    }

    public boolean merge(Deck from){
        Deck to = this;

        int tempSize = from.amount();

        for(int i = 0; i < tempSize; i++){
            to.getComponents().push(from.takeCard());
        }

        return true;
    }

    /**
     * Draw empty or line break
     */
    private void empty(){
        if(getComponents().isEmpty()){
            System.out.println("Empty of cards");
        }else{
            System.out.println("");

        }
    }

    @Override
    public boolean validate(Deck merge) {
        return true;
    }

}
