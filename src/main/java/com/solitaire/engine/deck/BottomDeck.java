package com.solitaire.engine.deck;

import com.solitaire.engine.deck.Deck;
import com.solitaire.engine.rules.StackRule;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 05.06.2015
 * Time: 11.22
 */
public class BottomDeck extends Deck {

    public BottomDeck(String name, int size) {
        super(name, size);
    }

    @Override
    public void draw() {
        super.draw();
    }

    @Override
    public boolean validate(Deck merge) {
        StackRule stackRule = new StackRule(this);
        return stackRule.validate(merge);
    }
}
