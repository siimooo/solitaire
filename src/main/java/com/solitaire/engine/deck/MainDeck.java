package com.solitaire.engine.deck;

import com.solitaire.engine.card.Card;
import com.solitaire.engine.deck.Deck;

/**
* Created by IntelliJ IDEA.
* User: Simo Ala-Kotila
* Date: 05.06.2015
* Time: 11.22
*/
public class MainDeck extends Deck {

    public MainDeck(String name, int size) {
        super(name, size);

    }

    @Override
    public void draw() {
        super.draw();
    }

    @Override
    public void expose() {
        int max =  amount() > 3 ? 3 : amount();
        boolean enqueue = true;

        for(int i = 0; i < max; i++){

            if(enqueue) {
                Card card = takeCard();
                card.setVisible(false);
                getComponents().enqueue(card);
            }else{
                showCard(amount() - (i + 1)).setVisible(true);
            }

            if(i == max - 1 && enqueue){
                i = 0;
                showCard(amount() - 1).setVisible(true);
                enqueue = false;
            }
        }
    }
}
