package com.solitaire.engine.deck;

import com.solitaire.engine.deck.Deck;
import com.solitaire.engine.rules.VictoryRule;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 16.6.2015
 * Time: 12.56
 */
public class SuitsDeck extends Deck {

    public SuitsDeck(String name, int size) {
        super(name, size);
    }

    @Override
    public boolean validate(Deck merge) {
        VictoryRule victoryRule = new VictoryRule(this);
        return victoryRule.validate(merge);
    }
}
