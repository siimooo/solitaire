package com.solitaire.engine.command;

import com.solitaire.engine.command.Command;
import com.solitaire.engine.deck.Deck;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 24.6.2015
 * Time: 14.09
 */
public class ClickCommand extends Command {

    private Deck mDeck;

    public ClickCommand(Deck deck){
        mDeck = deck;
    }

    @Override
    public void execute() {

        if(mDeck.isEmpty()){
            System.out.println("Can't expose because deck is empty");
            return;
        }

        mDeck.expose();
    }

    @Override
    public void undo() {

    }

    @Override
    public void redo() {

    }

    @Override
    public Action getAction() {
        return Action.CLICK_DECK;
    }
}
