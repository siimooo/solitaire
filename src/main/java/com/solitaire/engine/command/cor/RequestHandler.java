package com.solitaire.engine.command.cor;

import com.solitaire.engine.game.player.Player;
import com.solitaire.engine.game.table.Table;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 15.7.2015
 * Time: 11.10
 */
public abstract class RequestHandler {

    private RequestHandler mHandler;

    private static final String HELP_COMMAND = "!help";

    public RequestHandler(RequestHandler handler){
        mHandler = handler;
    }

    public void onHandleRequest(Request request){
        if(mHandler != null){
            mHandler.onHandleRequest(request);
        }
    }

    public boolean isRequest(String command, Request request){
        return command.equalsIgnoreCase(request.getRequest());
    }

    //TODO: move all of this above to IO class..
    public boolean cancel(String command){
        if(command.equalsIgnoreCase("cancel")){
            return true;
        }
        return false;
    }

    public boolean isInteger(String commnad){
        try{
            int integer = Integer.parseInt(commnad);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }

    public boolean isIndexValid(int index, int deckSize){
        return index <= deckSize && index >= 0;
    }

    public String read(){
        BufferedReader io = new BufferedReader(new InputStreamReader(System.in));
        try {
            return io.readLine();
        } catch (IOException e) {
            return null;
        }
    }

}
