package com.solitaire.engine.command.cor;

import com.solitaire.engine.deck.Deck;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 21.7.2015
 * Time: 10.50
 */
public class MachineMove extends RequestHandler {


    public MachineMove(RequestHandler handler) {
        super(handler);
    }

    @Override
    public void onHandleRequest(Request request) {
        if(isRequest("move", request)){
            boolean exit = false;
            String responseIndex = null;
            String responseFrom = null;
            String responseTo = null;

            int fromIndex = 0;
            int toIndex = 0;

            int decksSize = request.getTable().getDealer().getDecks().length;

            while(!exit){
                System.out.println("Select cards index");
                responseIndex = read();
                if(cancel(responseIndex)){
                    return;
                }
                System.out.println("Select a deck from");
                responseFrom = read();
                if(cancel(responseFrom)){
                    return;
                }
                System.out.println("Select a deck to");
                responseTo = read();
                if(cancel(responseTo)){
                    return;
                }

                if(!isInteger(responseIndex) || !isInteger(responseFrom) || !isInteger(responseTo)){
                    return;
                }

                fromIndex = Integer.parseInt(responseFrom);
                toIndex = Integer.parseInt(responseTo);

                if(isIndexValid(fromIndex, decksSize) && isIndexValid(toIndex, decksSize)){
                    exit = true;
                }
            }

            int index = Integer.parseInt(responseIndex);

            Deck[] decks = request.getTable().getDealer().getDecks();
            Deck from = decks[fromIndex];
            Deck to = decks[toIndex];

            request.getPlayer().moveCard(index, from, to);
        }else{
            super.onHandleRequest(request);
        }
    }
}
