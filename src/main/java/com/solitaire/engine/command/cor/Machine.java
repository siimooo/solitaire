package com.solitaire.engine.command.cor;

import com.solitaire.engine.game.player.HumanPlayer;
import com.solitaire.engine.game.table.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 17.7.2015
 * Time: 12.16
 */
public class Machine {

    private RequestHandler mChain;

    public Machine(){
        makeChain();
    }

    private void makeChain(){
        mChain = new MachinePrinter(new MachineMove(new MachineExpose(new MachineExit(null))));
    }

    public void makeRequest(Request request){
        mChain.onHandleRequest(request);
    }


}
