package com.solitaire.engine.command.cor;

import com.solitaire.engine.deck.Deck;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 22.7.2015
 * Time: 14.16
 */
public class MachineExit extends RequestHandler {
    public MachineExit(RequestHandler handler) {
        super(handler);
    }

    @Override
    public void onHandleRequest(Request request) {
        if(isRequest("exit", request) || isRequest("quit", request)){
            System.out.println("Game over. You quit the solitaire");
            System.exit(0);
        }else{
            super.onHandleRequest(request);
        }
    }
}
