package com.solitaire.engine.command.cor;

import com.solitaire.engine.deck.Deck;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 22.7.2015
 * Time: 10.48
 */
public class MachineExpose extends RequestHandler{

    public MachineExpose(RequestHandler handler) {
        super(handler);
    }

    @Override
    public void onHandleRequest(Request request) {
        if(isRequest("expose", request)){
            boolean exit = false;
            String response = null;

            int decksSize = request.getTable().getDealer().getDecks().length;
            int index = 0;
            while(!exit){
                System.out.println("Select a deck to expose");
                response = read();
                if(cancel(response)){
                    return;
                }

                if(!isInteger(response)){
                    return;
                }

                index = Integer.parseInt(response);
                if(isIndexValid(index, decksSize)){
                    exit = true;
                }
            }

            Deck deck = request.getTable().getDealer().getDecks()[index];
            request.getPlayer().clickDeck(deck);
        }else{
            super.onHandleRequest(request);
        }
    }
}
