package com.solitaire.engine.command.cor;

import com.solitaire.engine.game.player.HumanPlayer;
import com.solitaire.engine.game.table.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 15.7.2015
 * Time: 11.20
 */
public class MachinePrinter extends RequestHandler {

    public MachinePrinter(RequestHandler handler) {
        super(handler);
    }

    @Override
    public void onHandleRequest(Request request) {
        if(isRequest("draw", request)){
            request.getTable().draw();
        }else{
            super.onHandleRequest(request);
        }
    }
}
