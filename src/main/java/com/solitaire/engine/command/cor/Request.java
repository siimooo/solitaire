package com.solitaire.engine.command.cor;

import com.solitaire.engine.game.player.Player;
import com.solitaire.engine.game.table.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 15.7.2015
 * Time: 11.16
 */
public class Request {

    private final String mRequest;

    private Player mPlayer;

    private Table mTable;

    public Request(String request, Player player, Table table){
        mRequest = request;
        mPlayer = player;
        mTable = table;
    }
    
    public String getRequest(){
        return mRequest;
    }


    public Player getPlayer(){
        return mPlayer;
    }

    public Table getTable(){
        return mTable;
    }
}
