package com.solitaire.engine.command;

import com.solitaire.engine.command.Command;
import com.solitaire.engine.deck.Deck;

public class MoveCommand extends Command {

    private final int mFromIndex;
    private final Deck mFrom;
    private final Deck mTo;
    private Deck mMerge;

    public MoveCommand(int fromIndex, Deck from, Deck to){
        mFromIndex = fromIndex;
        mFrom = from;
        mTo = to;
    }

    @Override
    public void execute() {
        if(mFrom.isEmpty()){
            return;
        }

        boolean isPopPossible = (from().amount() - 1) - mFromIndex >= 0 ? true : false;

        if(!isPopPossible){
            System.out.println("Moving not possible. Not enough cards in deck");
            return;
        }

        mMerge = from().popsCard(mFromIndex);
        if(!mTo.validate(mMerge)){
            System.out.println("Invalid command.");
            undo();
            return;
        }

        mTo.merge(mMerge);
    }

    /**
     * Get selected stack where card is
     *
     * @return value of selected stack
     */
    public Deck from(){
        return mFrom;
    }

    /**
     * Get selected stack index
     *
     * @return value of selected index of stack
     */
    public int fromIndex(){
        return mFromIndex;
    }


    /**
     * Get where card is suppose to move
     *
     * @return value of stack
     */
    public Deck to(){
        return mTo;
    }

    @Override
    public void undo() {
        mFrom.merge(mMerge);
    }

    @Override
    public void redo() {

    }

    @Override
    public Action getAction() {
        return Action.MOVE_CARD;
    }
}
