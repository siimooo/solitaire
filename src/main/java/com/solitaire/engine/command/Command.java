package com.solitaire.engine.command;

/**
 * Using command pattern, because we want support undo and redo operator.
 *
 * @author Simo Ala-Kotila
 */
public abstract class Command {

    /**
     * Execute command
     *
     */
    public abstract void execute();

    /**
     * Undo command
     */
    public abstract void undo();

    /**
     * Redo command
     */
    public abstract void redo();


    public abstract Action getAction();


}
