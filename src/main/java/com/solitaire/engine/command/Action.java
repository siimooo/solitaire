package com.solitaire.engine.command;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 2.7.2015
 * Time: 10.35
 */
public enum Action {

    MOVE_CARD,
    CLICK_DECK;


}
