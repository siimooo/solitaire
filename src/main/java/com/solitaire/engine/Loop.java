package com.solitaire.engine;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 10.7.2015
 * Time: 10.44
 */
public class Loop {

    public void loop(){
        long end = ending();
        while(System.currentTimeMillis() < end){
            synchronized (this){
                    waiting(end);
                    end = ending();
                    System.out.print("*");
            }
        }
    }

    public long ending(){
        return System.currentTimeMillis() + 500;
    }

    public void waiting(long end){
        try {
            wait(end - System.currentTimeMillis());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
