package com.solitaire.contract;

import com.solitaire.engine.card.Card;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 9.6.2015
 * Time: 11.19
 */
public interface Visitor {

    /**
     * Visit in concrete card object
     *
     * @param card of concrete
     */
    void visit(Card card);
}
