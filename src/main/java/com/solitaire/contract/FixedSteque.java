package com.solitaire.contract;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.Random;

/**
 * LIFO (last-in-first-out) and enqueue Steque with generic implementation.
 *
 * @param <T> generic item in stequeue
 * @author Simo Ala-Kotila
 */
public class FixedSteque<T> implements Iterable<T> {

    /**
     * Hold this stack max size
     */
    private final int mCap;

    /**
     * Generic class of T
     */
    private final Class<T> mC;

    /**
     * Size of this steque
     */
    private int mSize;

    /**
     * Keep track where should place next item
     */
    private int mRight;

    /**
     * Hold stack data structure here
     */
    private T[] mSteque;


    /**
     * Stack with max cap size
     *
     * @param c
     * @param cap size of this stack
     */
    public FixedSteque(Class<T> c, int cap) {
        mC = c;
        mCap = cap;
        mSteque = (T[]) Array.newInstance(mC, mCap);
    }

    public boolean isEmpty() {
        return mSize <= 0;
    }

    public boolean isFull() {
        return mSize >= mCap;
    }

    public int size() {
        return mSize;
    }

    /**
     * Peek item from stack. Peek won't remove item from stack
     *
     * @return
     */
    public T peek() {
        if (isEmpty()) {
            throw new IllegalArgumentException("Can't peek because steque is currently empty");
        }
        return mSteque[mSize - 1];
    }

    /**
     * Pop most recent added item from stack
     *
     * @return item of stack
     */
    public T pop() {
        if (isEmpty()) {
            throw new IllegalArgumentException("Can't pop because steque is currently empty");
        }

        mSize--;
        mRight--;

        T item = mSteque[mRight];
        mSteque[mRight] = null; //release for gc


        return item;
    }

    public void enqueue(T item){
        if(isFull()){
            throw new IllegalArgumentException("Can't enqueue because steque if currently full");
        }

        shift(item);
        mSize++;
        mRight++;
    }

    private void shift(T item){
        for(int i = size() - 1; i >= 0;i--){
            mSteque[i + 1] = mSteque[i];
        }
        mSteque[0] = item;
    }

    public void shuffle() {
        if (isEmpty()) {
            throw new IllegalArgumentException("Can't shuffle because steque is currently empty");
        }

        int min = 0;
        int max = mSize - 1;
        Random random = new Random(System.currentTimeMillis());

        for (int i = 0; i < size(); i++) {
            int rand = random.nextInt((max - min) + 1);
            T swap = mSteque[rand];
            mSteque[rand] = mSteque[i];
            mSteque[i] = swap;
        }
    }

    public T get(int index){
        if(isEmpty()){
            throw new IllegalArgumentException("Can't get because steque is currently empty");
        }

        if(index >= mSize){
            throw new IllegalArgumentException("Can't get because steque has less elements than wanted");
        }

        return mSteque[index];
    }

    /**
     * Push new item to stack
     *
     * @param item
     */
    public void push(T item) {
        if (isFull()) {
            throw new IllegalArgumentException("Can't push because steque is currently full");
        }
        mSteque[mRight] = item;
        mSize++;
        mRight++;
    }


    public Iterator<T> iterator() {
        return new ReverseArrayIterator();
    }

    private class ReverseArrayIterator implements Iterator<T> {
        //private int i; //asc
        private int i = mSize; //desc

        public boolean hasNext() {
            //return i < mPosition; //asc
            return i > 0; //desc
        }

        public T next() {
            i--; //desc
            //i++; //asc
            return mSteque[i];
        }

        public void remove() {
            throw new UnsupportedOperationException("This stack has no implementation for remove");
        }
    }
}
