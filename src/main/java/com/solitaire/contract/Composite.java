package com.solitaire.contract;

/**
 * Abstract "lowest common denominator"
 *
 * Created by Simo on 01.06.2015.
 */
public abstract class Composite implements Component {

    private FixedSteque<Component> mComponent;

    private final String mName;

    public Composite(String name, int size){
        mName = name;
        mComponent = new FixedSteque<Component>(Component.class, size);
    }

    public void addComponent(Component component){
        mComponent.push(component);
    }

    public FixedSteque<Component> getComponents(){
        return mComponent;
    }

    public Component getComponent(int index){
        return mComponent.get(index);
    }

    public String getName(){
        return mName;
    }
}
