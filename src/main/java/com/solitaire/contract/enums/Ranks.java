package com.solitaire.contract.enums;
/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 25.05.2015
 * Time: 13.40
 */
public enum Ranks {

    ACE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    JACK(11),
    QUEEN(12),
    KING(13),
    JOKER(15);

    private final int mValue;

    Ranks(int value) {
        mValue = value;
    }

    public int getValue(){
        return mValue;
    }

}
