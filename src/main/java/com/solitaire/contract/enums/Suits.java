package com.solitaire.contract.enums;

/**
 * Representation game suits
 *
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 25.05.2015
 * Time: 13.40
 */
public enum Suits {
    HEARTS("♥"),
    DIAMONDS("♦"),
    CLUBS("♣"),
    JOKER("Joker"),
    SPADES("♠");

    private final String mValue;

    Suits(String value){
        mValue = value;
    }

    public String getValue(){
        return mValue;
    }

}
