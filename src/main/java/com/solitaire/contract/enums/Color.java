package com.solitaire.contract.enums;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 25.05.2015
 * Time: 13.40
 */
public enum Color {

    RED("Red"), BLACK("Black");

    private final String mValue;

    Color(String value){
        mValue = value;
    }

    public String getValue(){
        return mValue;
    }
}
