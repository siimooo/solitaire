package com.solitaire.contract;

/**
 * Define "lowest common denominator"
 *
 * Created by Simo on 01.06.2015.
 */
public interface Component{

    /**
     * Draw component
     */
    void draw();

    /**
     * Accept visit to concrete class
     *
     * @param visitor implement
     */
    void accept(Visitor visitor);

    void detectOS();
}
