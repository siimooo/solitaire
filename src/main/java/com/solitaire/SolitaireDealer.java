package com.solitaire;

import com.solitaire.engine.card.Card;
import com.solitaire.engine.deck.BottomDeck;
import com.solitaire.engine.deck.Deck;
import com.solitaire.engine.deck.MainDeck;
import com.solitaire.engine.deck.SuitsDeck;
import com.solitaire.engine.game.Dealer;

/**
 * Created by IntelliJ IDEA.
 * User: Simo Ala-Kotila
 * Date: 01.06.2015.
 * Time: 13.40
 */
public class SolitaireDealer extends Dealer {

    private boolean mIsGameOver = false;

    @Override
    public void setWinner() {
        mIsGameOver = true;
    }

    @Override
    public void fill(Deck deck) {
//        int count = mKlondike.getDisplay().getDisplayCount();
//        count = 23 - count;

        int count = 3;
        count = 23 - count;

        for(int x = 0; x < 12; x++){
            if(x < 7){//fill bottom stacks
                int j = 0;
                Deck leaf = new BottomDeck("Bottom Deck "+ x, 52);
                while(j < x + 1){
                    Card card = deck.takeCard();
                    if(j > x - 1){//set last card to visible
                        card.setVisible(true);
                    }
                    leaf.addComponent(card);
                    j++;
                }
                getDecks()[x] = leaf;
            }

            if(x > 10){//fill "main" stack
                Deck main = new MainDeck("MainCommand Deck", 24);
                for(int i = 0; i < 24; i++) {
                    Card card = deck.takeCard();
                    main.addComponent(card);
                    if(i > count){
                        card.setVisible(true);
                    }
                }
                getDecks()[x] = main;
            }else if(x > 6){//right empty stacks
                Deck empty = new SuitsDeck("Empty Deck " + x, 13);
                getDecks()[x] = empty;
            }
        }
    }

    @Override
    public Deck setUp() {
        setDecks(new Deck[12]);

        Deck deck = buildCardDeck();
        deck.getComponents().shuffle();
        deck.getComponents().shuffle();
        return deck;
    }

    @Override
    public boolean isWinner() {
        return mIsGameOver;
    }

    @Override
    public void playerIn() {

    }

    @Override
    public void playersCheck() {

    }
}
