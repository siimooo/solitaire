package com.solitaire;

import com.solitaire.engine.command.Action;
import com.solitaire.engine.command.Command;
import com.solitaire.engine.game.Dealer;
import com.solitaire.engine.game.player.PartyMember;
import com.solitaire.engine.rules.Score;
import com.solitaire.engine.game.table.Table;
import com.solitaire.engine.deck.Deck;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SolitaireTable extends Table{

    @Override
    public void draw() {
        int index = 0;
        getAbstractTableColor().clear();
        for(Deck deck : getDealer().getDecks()){
            if(index == 11){
                System.out.printf("-%s-%n", "Main Deck");
            }else if(index == 7){
                System.out.printf("-%s-%n", "Empty foundations");
            }else if(index == 0){
                System.out.printf("-%s-%n", "Bottom Decks");
            }
            System.out.printf("%1s ",index + ".");
            deck.draw();
            index++;
        }
    }

    @Override
    public void act(PartyMember sender, Action action) {
        Deck[] decks = getDealer().getDecks();
        if(action == Action.MOVE_CARD){
            int one = decks[7].amount();
            int two = decks[8].amount();
            int three = decks[9].amount();
            int four = decks[10].amount();
            if(one == 13 && two == 13 && three == 13 && four == 13){
                System.out.println("GameOver!");
                getDealer().setWinner();
            }
        }
    }


}