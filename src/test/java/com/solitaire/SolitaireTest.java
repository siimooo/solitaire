package com.solitaire;

import com.solitaire.engine.deck.MainDeck;
import com.solitaire.engine.deck.BottomDeck;
import com.solitaire.engine.deck.SuitsDeck;
import com.solitaire.engine.card.*;
import com.solitaire.contract.*;
import com.solitaire.contract.Component;
import com.solitaire.contract.enums.Ranks;
import com.solitaire.contract.FixedSteque;
import com.solitaire.engine.command.*;
import com.solitaire.engine.deck.Deck;
import com.solitaire.engine.game.player.HumanPlayer;
import com.solitaire.engine.game.player.Player;
import com.solitaire.engine.game.table.Table;
import com.solitaire.engine.platform.AbstractPlatform;
import com.solitaire.engine.rules.MainRule;
import com.solitaire.engine.rules.Rule;
import com.solitaire.engine.rules.VictoryRule;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class SolitaireTest {

    @Test
    public void stackPeek() {

        FixedSteque<String> peek = new FixedSteque<String>(String.class, 10);
        peek.push("a");
        peek.push("b");

        String b = peek.peek();
        assertTrue(b, true);
    }

    @Test
    public void stackPush() {
        FixedSteque<String> push = new FixedSteque<String>(String.class, 10);
        push.push("a");
        push.push("b");

        boolean condition = push.size() == 2;
        assertTrue(condition);
    }

    @Test
    public void stackPop() {

        FixedSteque<String> pop = new FixedSteque<String>(String.class, 10);
        pop.push("a");
        pop.push("b");

        String b = pop.pop();
        pop.push("e");
        String e = pop.pop();
        assertTrue(((b != null && b.equals("b")) && (e != null && e.equals("e"))));
    }

    @Test
    public void stackIterator() {
        FixedSteque<String> iterator = new FixedSteque<String>(String.class, 10);
        iterator.push("a");
        iterator.push("c");
        iterator.pop();
        iterator.push("e");
        iterator.push("f");
        iterator.pop();
        for (String string : iterator) {
            assertNotNull(string);
        }
        assertTrue(iterator.size() == 2);
    }

    @Test
    public void stackShuffle() {
        FixedSteque<String> stack = new FixedSteque<String>(String.class, 10);
        stack.push("a");
        stack.pop();
        stack.push("c");
        stack.pop();
        stack.push("e");
        stack.pop();
        stack.push("f");
        stack.push("h");
        stack.shuffle();

        for (String string : stack) {
            assertNotNull(string);
        }
        assertTrue(stack.size() == 2);
    }

    @Test
    public void testDeckCard() {
        //make root deck. solitaire need 13 cards stack
        Deck[] solitaire = new Deck[2];

        //make some cards
        Spades spadeAce = new Spades(Ranks.ACE);
        Spades spadeFive = new Spades(Ranks.FIVE);
        Hearts heartKing = new Hearts(Ranks.KING);
        Clubs clubs = new Clubs(Ranks.FOUR);
        Diamonds diamonds = new Diamonds(Ranks.KING);

        assertNotNull(spadeAce.color());
        assertNotNull(spadeFive.color());
        assertNotNull(heartKing.color());
        assertNotNull(clubs.color());
        assertNotNull(diamonds.color());

        assertNotNull(spadeAce.suits());
        assertNotNull(spadeFive.suits());
        assertNotNull(heartKing.suits());
        assertNotNull(clubs.suits());
        assertNotNull(diamonds.suits());

        assertNotNull(spadeAce.ranks());
        assertNotNull(spadeFive.ranks());
        assertNotNull(heartKing.ranks());
        assertNotNull(clubs.ranks());
        assertNotNull(diamonds.ranks());


//        //make new leaf -> split deck
        Deck split1 = new Deck("leaf one", 13);

        //make new leaf -> split deck
        Deck split2 = new Deck("leaf two", 13);

        split2.addComponent(spadeFive);//add to leaf 2
        split1.addComponent(heartKing);//add to leaf 1
        split2.addComponent(spadeAce);//add to leaf 2
        split2.addComponent(clubs);
        split1.addComponent(diamonds);


        assertTrue(split2.getComponents().size() == 3);
        assertTrue(split1.getComponents().size() == 2);

        solitaire[0] = split1;//add to root
        solitaire[1] = split2;//add to root

        assertTrue(solitaire.length == 2);
    }


    @Test
    public void testSolitaireDealer(){
        SolitaireDealer solitaire = new SolitaireDealer();
        Deck[] deck = solitaire.getDecks();
        assertTrue(deck.length == 12);
        for(Deck singleStack : deck){
            for(Component component : singleStack.getComponents()){
                assertNotNull(component);
            }
        }
    }

    @Test
    public void testSolitaireDealerMainDeck(){
        SolitaireDealer solitaire = new SolitaireDealer();
        Deck[] decks = solitaire.getDecks();
        Deck main = decks[11];
        int count = 3;
        count = 24 - count;
        for(int i = count; i < 24; i++){
            Component component = main.getComponent(i);
            CardVisitor visitor = new CardVisitor();
            component.accept(visitor);
            assertTrue(visitor.getCard().isVisible());
        }
    }

	@Test
	public void buildSolitaireTable(){
        Table table = new SolitaireTable();
        table.setDealer(new SolitaireDealer());
        assertNotNull(table);
        for(Deck singleStack : table.getDealer().getDecks()){
            for(Component component : singleStack.getComponents()){
                assertNotNull(component);
            }
        }
        table.draw();
	}


    @Test
    public void visitCard(){
        Hearts heartsKing = new Hearts(Ranks.KING);
        Visitor visitor = new CardVisitor();
        heartsKing.accept(visitor);
    }


    @Test
    public void visitDeckCard(){
        Deck deck = new Deck("Deck", 13);
        deck.addComponent(new Hearts(Ranks.KING));
        deck.addComponent(new Clubs(Ranks.FIVE));
        Visitor visitor = new CardVisitor();
        deck.accept(visitor);
        deck.accept(visitor, 1);
    }

    @Test
    public void testCardVisible(){
        Joker joker = new Joker();
        assertFalse(joker.isVisible());
        joker.setVisible(true);
        assertTrue(joker.isVisible());
    }

    @Test
    public void testModuloWrapAround(){
        int max = 10;
        int modulo =  10 % max;
        assertTrue(modulo == 0);
    }

    @Test
    public void testEnqueue2(){
        FixedSteque<String> a = new FixedSteque<String>(String.class, 3);
        a.enqueue("a");
        a.enqueue("b");
        a.enqueue("c");

        assertTrue(a.pop().equals("a"));
    }

    @Test
    public void testMerge(){
        Deck from = new Deck("Move stack", 13);
        Component heartsFive = new Hearts(Ranks.FIVE,true);//merge won't take this
        from.addComponent(heartsFive);
        from.addComponent(new Hearts(Ranks.ACE, false));
        from.addComponent(new Hearts(Ranks.TEN, true));

        Deck to = new Deck("Empty stack", 13);
        to.addComponent(new Diamonds(Ranks.ACE, true));

        Deck merge = from.popsCard(1);

        to.merge(merge);

        for(Component comp : to.getComponents()){
            assertNotEquals(heartsFive, comp);
        }

        assertTrue(to.getComponents().size() == 3);
        assertTrue(from.getComponents().size() == 1);

    }

    @Test
    public void deckSize(){
        Deck deck = new Deck("Deck Size", 3);
        deck.addComponent(new Hearts(Ranks.FIVE));
        deck.addComponent(new Hearts(Ranks.ACE));
        deck.addComponent(new Hearts(Ranks.KING));
        assertTrue(deck.amount() == 3);
    }

    @Test
    public void deckGiveCard(){
        Deck deck = new Deck("Deck Size", 3);
        deck.addComponent(new Hearts(Ranks.FIVE));
        deck.addComponent(new Hearts(Ranks.ACE));
        deck.addComponent(new Hearts(Ranks.KING));
        Card card = deck.takeCard();

        assertTrue(card.ranks() == Ranks.KING);
        assertTrue(deck.amount() == 2);
    }

    @Test
    public void deckShowCard(){
        Deck deck = new Deck("Deck Size", 3);
        deck.addComponent(new Hearts(Ranks.FIVE));
        deck.addComponent(new Hearts(Ranks.ACE));
        deck.addComponent(new Hearts(Ranks.KING));
        Card card = deck.showCard();

        assertTrue(card.ranks() == Ranks.KING);
        assertTrue(deck.amount() == 3);
    }

    @Test
    public void deckShowCardByIndex(){
        Deck deck = new Deck("Deck Size", 3);
        deck.addComponent(new Hearts(Ranks.FIVE));
        deck.addComponent(new Hearts(Ranks.ACE));
        deck.addComponent(new Hearts(Ranks.KING));
        Card card = deck.showCard(1);

        assertTrue(card.ranks() == Ranks.ACE);
        assertTrue(deck.amount() == 3);
    }



    @Test
    public void testMoveCard(){
        Deck from = new Deck("From", 13);
        from.addComponent(new Spades(Ranks.JACK, true));
        from.addComponent(new Spades(Ranks.NINE, true));

        Deck to = new Deck("To", 13);
        to.addComponent(new Hearts(Ranks.TEN, true));

        MoveCommand move = new MoveCommand(0, from, to);
        move.execute();

        to.draw();
//        assertTrue(from.getComponents().size() == 1);//from
//        assertTrue(to.getComponents().size() == 2);//to
    }

    @Test
    public void validateMainRule(){
        Deck merge = new Deck("Merge", 2);
        merge.addComponent(new Hearts(Ranks.QUEEN, true));

        Deck to = new Deck("To", 5);
        to.addComponent(new Spades(Ranks.KING, true));
        Rule rule = new MainRule(to);
        assertTrue(rule.validate(merge));

        Deck merge2 = new Deck("Merge", 2);
        merge2.addComponent(new Diamonds(Ranks.QUEEN, true));

        assertTrue(rule.validate(merge2));
        assertTrue(rule.isLowerByOneRank(new Spades(Ranks.QUEEN), new Hearts(Ranks.KING)));
        assertTrue(rule.isSameColor(new Spades(Ranks.KING), new Spades(Ranks.QUEEN)));
        assertTrue(rule.isSameSuits(new Hearts(Ranks.KING), new Hearts(Ranks.QUEEN)));
    }

    @Test
    public void testCompareTo(){
        Card spades = new Spades(Ranks.JACK);
        Card diamonds = new Diamonds(Ranks.SEVEN);
        Card hearts = new Hearts(Ranks.JACK);
        int less = diamonds.compareTo(spades);
        int higher = spades.compareTo(diamonds);
        int equal = hearts.compareTo(spades);
        assertTrue(less == -1);
        assertTrue(higher == 1);
        assertTrue(equal == 0);
    }

    @Test
    public void testPlayer(){
        Deck from = new BottomDeck("From", 13);
        from.addComponent(new Hearts(Ranks.TEN, true));
        from.addComponent(new Spades(Ranks.QUEEN, true));
        from.addComponent(new Hearts(Ranks.JACK, true));

        Deck to = new BottomDeck("To", 13);
        to.addComponent(new Spades(Ranks.QUEEN, true));

        HumanPlayer player = new HumanPlayer("Simo");
        player.moveCard(0, from, to);

        assertTrue(from.amount() == 2);
        assertTrue(to.amount() == 2);
    }


    @Test
    public void testExposeMethod(){
        Deck deck = new Deck("Expose", 2);
        deck.addComponent(new Spades(Ranks.NINE));
        deck.addComponent(new Hearts(Ranks.ACE));
        deck.expose();
//        deck.draw();
        Card card = deck.showCard();
        assertTrue(card.isVisible());
    }


    @Test
    public void testClickCommand(){
        Deck deck = new Deck("Expose", 2);
        deck.addComponent(new Spades(Ranks.TEN));

        Player player = new HumanPlayer("simo");
        player.clickDeck(deck);

        assertTrue(deck.showCard().isVisible());
    }

    @Test
    public void testVictory(){
        Deck merge = new Deck("From", 13);
        merge.addComponent(new Hearts(Ranks.TEN, true));

        Deck to = new SuitsDeck("Suits Deck", 13);

        Rule victory = new VictoryRule(to);
        assertFalse(victory.validate(merge));
        merge.takeCard();

        merge.addComponent(new Hearts(Ranks.ACE, true));
        assertTrue(victory.validate(merge));

        Command command = new MoveCommand(0, merge, to);
        command.execute();

        merge.addComponent(new Hearts(Ranks.TWO, true));
        assertTrue(victory.validate(merge));

        Command command2 = new MoveCommand(0, merge, to);
        command2.execute();

        merge.addComponent(new Spades(Ranks.THREE, true));
        assertFalse(victory.validate(merge));
        merge.takeCard();

        merge.addComponent(new Hearts(Ranks.FOUR, true));
        assertFalse(victory.validate(merge));
    }

    @Test
    public void testPlatform(){
        System.out.println(AbstractPlatform.getOperatingSystemType());

    }

    @Test
    public void testParty(){
        Table table = new SolitaireTable();
        table.setDealer(new SolitaireDealer());
        Player human = new HumanPlayer("Simo");
        table.addMember(human);

        Deck[] decks = table.getDealer().getDecks();
        human.moveCard(0, decks[0], decks[7]);
    }

    @Test
    public void testMainDeckExpose(){
        Deck main = new MainDeck("Main", 24);
        main.addComponent(new Hearts(Ranks.TWO));
        main.addComponent(new Spades(Ranks.ACE));
        main.addComponent(new Diamonds(Ranks.FOUR));
        main.addComponent(new Hearts(Ranks.EIGHT));
        main.addComponent(new Clubs(Ranks.QUEEN, true));
        main.addComponent(new Diamonds(Ranks.SEVEN, true));
        main.addComponent(new Spades(Ranks.KING, true));

        main.draw();

        main.expose();
        main.draw();

        main.expose();
        main.draw();

        main.expose();
        main.draw();
    }


    @Test
    public void testPopPossible(){
        Deck main = new MainDeck("Main", 24);
        main.addComponent(new Hearts(Ranks.TWO));

        boolean isPopPossible = (main.amount() - 1) - 1 >= 0 ? true : false;

    }


    @Test
    public void testAbstractPlatform(){

    }

    @Test
    public void testHandler(){
//        String ui = "move";
//        Deck deck = new Deck("Asd", 12);
//        deck.addComponent(new Hearts(Ranks.THREE));
//        Command command = new ClickCommand(deck);
//        MachinePrinter handler = new MachinePrinter(command);
//        handler.onHandleRequest(new Request(ui));
    }



}